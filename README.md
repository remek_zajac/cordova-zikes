# Cordova-Zikes #

This is the Cordova-Zikes project - the mobile client(s) of [Zikes](http://zikes.website).


## Setup ##

* [Install Cordova](https://cordova.apache.org/docs/en/latest/guide/cli/) & add platforms (adding them soils the source/git, so they're kept out of this repo)

## Release ##

cordova run --release android

### Signing Android App ###
```
run android --release -- --keystore=/home/remek/.ssh/zikes_android_key/key --storePassword=xxx --alias=zikes_android_generic --password=xxx
```